package com.example.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 当Spring Boot应用启动时，它会自动从配置文件中读取pattern.dateformat、pattern.envSharedValue和pattern.name的值
 * 并设置到PatternProperties类的对应字段上
 * 在应用的其他地方，你可以通过注入PatternProperties Bean来获取这些配置属性的值
 */
@Data
@Component
@ConfigurationProperties(prefix = "pattern")
public class PatternProperties {
    private String dateformat;
    private String envSharedValue;
    private String name;
}
