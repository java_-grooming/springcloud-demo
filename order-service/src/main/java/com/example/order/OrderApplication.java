package com.example.order;

import com.example.feign.clients.UserClient;
import com.example.feign.config.DefaultFeignConfiguration;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@MapperScan("com.example.order.mapper")
@SpringBootApplication
@EnableFeignClients(clients = UserClient.class,defaultConfiguration = DefaultFeignConfiguration.class)
// 这个注解启用Feign客户端，使得可以通过声明式的方式调用远程服务
// clients属性指定了哪些接口被注册为Feign客户端，defaultConfiguration属性指定了Feign客户端的默认配置类
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    /**
     * 创建RestTemplate并注入Spring容器
     * RestTemplate是Spring提供的一个用于发起HTTP请求的同步客户端
     * 给RestTemplate实例添加负载均衡的能力，它会根据服务名称来选择合适的实例
     * 在与Eureka或其他服务发现工具结合使用时，这允许RestTemplate通过逻辑服务名称（而不是具体的URL）来调用服务
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

   @Bean // 创建Ribbon的负载均衡规则, 指定使用随机选择算法
    public IRule randomRule() {
        return new RandomRule();
    }
}