package com.example.order.pojo;

import com.example.feign.pojo.User;
import lombok.Data;
//import com.example.user.pojo.User;

@Data
public class Order {
    private Long id;
    private Long price;
    private String name;
    private Integer num;
    private Long userId;
    private User user;
}