package com.example.feign.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;

/**
 * 当你在Feign客户端上应用这个配置类时，Feign会按照BASIC级别来记录日志
 * 这样你就可以看到Feign发送的HTTP请求的方法和URL，以及收到的HTTP响应的状态码和执行时间
 */
public class DefaultFeignConfiguration {
    @Bean
    public Logger.Level logLevel(){
        return Logger.Level.BASIC;
    }
}
