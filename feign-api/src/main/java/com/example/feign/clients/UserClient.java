package com.example.feign.clients;


import com.example.feign.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 这个接口的作用是，当你调用findById方法的时候，Feign会在网络上发起一个到"userservice"服务的HTTP请求
 * 请求路径是"/user/"加上你传入的id值，然后等待并处理响应
 */

@FeignClient(value = "userservice")  // 注解这是一个Feign客户端，"userservice"是请求服务名
public interface UserClient {

    @GetMapping("/user/{id}")  // 定义HTTP请求是一个GET请求，请求的路径是"/user/{id}"
    User findById(@PathVariable("id") Long id);
}
